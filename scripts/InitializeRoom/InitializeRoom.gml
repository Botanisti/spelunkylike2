/// @ Initilialize our Room
randomize();

gridSize = 32; // Size of your blocks and objects in game

xSections = 4; // how many rooms along the x axis
ySections = 4; // how many rooms along the y axis
sections = []; // Declare out rooms array

sectionTilesX = 10; // how many tiles wide our section is
sectionTilesY = 8; // How many to;es high our section is

sectionWidth = sectionTilesX * gridSize; // Widht of 1 section in pixels
sectionHeight = sectionTilesY * gridSize; // Height of 1 section in pixels

room_width = (sectionWidth * xSections) + (gridSize * 2);
room_height = (sectionHeight * ySections) + (gridSize * 2);

for (_y = 0; _y < ySections; _y++)
	for (_x = 0; _x < xSections; _x++)
		sections [_x, _y] = 0;
		
// Create main path
CreateMainPath();

for  (_y = 0; _y < ySections; _y++)
	show_debug_message(string(sections[0, _y]) + string(sections[1, _y]) + string(sections[2, _y]) + string(sections[3, _y]));
  

//initializing our sections (loading sections as strings into memory)
InitializeAllSections();
InitiliazeAllChunks();

//Create out level
GenerateLevel ();

//Clean up memory (De-initialize our sections)
ds_list_destroy(sectionLeftBottomRightList);
ds_list_destroy(sectionLeftRigthList);
ds_list_destroy(sectionLeftTopRightList);
ds_list_destroy(chunks);

