var _currentXSection	= argument0;
var _currentYSection	= argument1;
var _sectionStringData	= argument2;
var TypeOfObject;

for (c = 1; c < string_length(_sectionStringData) + 1; c++)
{
	switch (string_char_at(_sectionStringData, c))
	{
		case "0":
			continue;
		case "1":
			TypeOfObject = oBlock;
			break;
		case "R":
		{
			if (choose(true, false))
			{
				TypeOfObject = oRandomBlock;
				break;
			}
			else
				continue;
		}
		case "C":
			TypeOfObject = oChunk;
			break;
			

	}
	instance_create_layer((_currentXSection * sectionWidth) + (((c - 1) mod sectionTilesX) * gridSize) + gridSize,
					  (_currentYSection * sectionHeight) + (floor((c - 1) / sectionTilesX) * gridSize) + gridSize,
						"Instances", TypeOfObject);
			
}