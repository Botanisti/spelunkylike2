{
    "id": "2511c9f1-3a51-4845-9d38-0010f625aa14",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRandomBlock",
    "eventList": [
        {
            "id": "9cbdb5e7-0f6c-4621-87c5-22dc50e128bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2511c9f1-3a51-4845-9d38-0010f625aa14"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "de1a309c-9f27-42e7-9057-0bf132639cc6",
    "visible": true
}