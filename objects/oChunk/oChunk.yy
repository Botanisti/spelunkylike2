{
    "id": "f3590c38-6d1d-4515-b2b7-e2329fa8630c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChunk",
    "eventList": [
        {
            "id": "53ca6996-e809-440d-8adb-dfdd97db90e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3590c38-6d1d-4515-b2b7-e2329fa8630c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de1a309c-9f27-42e7-9057-0bf132639cc6",
    "visible": true
}