{
    "id": "de1a309c-9f27-42e7-9057-0bf132639cc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4f1ab42-970c-4b8b-bac7-2930863fa312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de1a309c-9f27-42e7-9057-0bf132639cc6",
            "compositeImage": {
                "id": "468f6d26-1e88-4bc4-bc7b-13b2af67f523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f1ab42-970c-4b8b-bac7-2930863fa312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "102708dc-8071-4ee5-91fb-13768d52681f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f1ab42-970c-4b8b-bac7-2930863fa312",
                    "LayerId": "c0cc5506-6eae-4ffa-bcb8-6c365a74d566"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c0cc5506-6eae-4ffa-bcb8-6c365a74d566",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de1a309c-9f27-42e7-9057-0bf132639cc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}